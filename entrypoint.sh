#!/bin/sh

# Install the paired launcher.
if "${INSTALL_LAUNCHER:-false}"
then
    cp /usr/local/bin/launch /install/launch-ansible
    chmod +x /install/launch-ansible
    chown "${USER_ID}":"${GROUP_ID}" /install/launch-ansible
    echo "
'launch-ansible' was correctly installed. Please run launch-ansible --help for more info.
Thank you!
Bye...
"
    exit 0
fi

cmd2install_launch='mkdir -p "$HOME/bin" && docker run --rm -e INSTALL_LAUNCHER=true -e USER_ID="$(id -u)" -e GROUP_ID="$(id -g)" -v "$HOME/bin:/install" gbarre2/docker-ansible'

# Check if the container via run via the launcher.
if [ -z "${WHOLAUNCHME}" ]
then
    echo "
WARNING: to use this image correctly, you have to run it via the paired launch script.
To install this launch script, run the following command:

    ${cmd2install_launch}

Bye...
"
    exit 0

fi

# Check is the launcher is the paired launcher.
if [ "${WHOLAUNCHME}" != "__SHA256SUM__" ]
then
    echo "
WARNING: to use this image correctly, you have to update your paired launch script.
Please, run the following command:

    ${cmd2install_launch}

Thank you :-D
Bye...
"
    exit 0
fi

# Create ansible user
addgroup --gid "${GROUP_ID}" ansible >/dev/null 2>&1
adduser --shell /bin/bash --no-create-home --home /home/ansible --disabled-password --gecos "Ansible" ansible --uid "${USER_ID}" --gid "${GROUP_ID}" >/dev/null 2>&1

# Add ansible account in the pyenv group.
gpasswd --add ansible pyenv >/dev/null 2>&1

# Set the correct owner in /home/ansible.
chown ansible:ansible /home/ansible /home/ansible/.bash_history

# Hack for ansible vault
chown ansible:ansible /var/run/ansible

# Hack for .bashrc and .vimrc.
for f in .bashrc .vimrc
do
    cp "/etc/skel/$f" /home/ansible/
    chown ansible:ansible "/home/ansible/$f"
done

cat >>/home/ansible/.bashrc <<'EOF'

# Setting of environment variable ANSIBLE_VAULT_PASSWORD_FILE.
printf '%s\n' "$ANSIBLE_VAULT_PASSWORD" > /var/run/ansible/vault
chmod 0600 /var/run/ansible/vault
export ANSIBLE_VAULT_PASSWORD_FILE="/var/run/ansible/vault"
unset ANSIBLE_VAULT_PASSWORD

PS1="${debian_chroot:+($debian_chroot)}"'[$?]'" \e[36m\u@ansible\e[00m \e[35m\A\e[00m \e[38;5;202m\w\e[00m\n\$ "

. "/usr/local/share/pyenv-ansible-${ANSIBLE_VERSION}/bin/activate"
eval $(register-python-argcomplete ansible)
eval $(register-python-argcomplete ansible-config)
eval $(register-python-argcomplete ansible-console)
eval $(register-python-argcomplete ansible-doc)
eval $(register-python-argcomplete ansible-galaxy)
eval $(register-python-argcomplete ansible-inventory)
eval $(register-python-argcomplete ansible-playbook)
eval $(register-python-argcomplete ansible-pull)
eval $(register-python-argcomplete ansible-vault)
cd /home/ansible/src

# Setting of aliases.
alias a="ansible"
alias ap="ansible-playbook"
alias vim='vim -p'
alias vi='vim -p'
alias l='ls --color -lp'
alias ll='ls --color -lap'
alias tree='tree -CF --dirsfirst -a'
alias grep='grep --color'
alias rgrep='grep -rn --color'

# Cosmetic.
echo

EOF

if [ ! -z "${SSH_USER}" ]; then
  cat >>/home/ansible/.bashrc <<EOF
# Setting of aliases.
alias ansible="ansible -u${SSH_USER} "
alias ansible-playbook="ansible-playbook -u${SSH_USER} "

echo "WARNING: 'ansible' and 'ansible-playbook' are aliased
==> ansible and a           aliases for 'ansible          -u${SSH_USER} '
==> ansible-playbook and ap aliases for 'ansible-playbook -u${SSH_USER} '
"

EOF
fi

if [ ! -z "${CHECK_VERSION}" ]; then
  cat >>/home/ansible/.bashrc <<EOF
# Lanch check script
check_version

EOF
fi

# Run container as ansible
exec sudo --user ansible --preserve-env --set-home /bin/bash
