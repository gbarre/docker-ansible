# Examples of usual Ansible commands.
ansible all --user FOO -m setup --ssh-common-args='-o StrictHostKeyChecking=no'
ansible all --user FOO -m setup --ssh-common-args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
ansible all --user FOO -m setup
ansible foo-srv-1 --user FOO -m debug -a 'msg={{group_names}}' # To list groups of a specific host.
ansible-playbook path/to/my/playbook.yml --user root --ask-pass --check
ansible-playbook path/to/my/playbook.yml --user FOO --tags BAR --limit "gr_foobar" --check
ansible-vault encrypt_string
printf '%s' 'mypassword' | ansible-vault encrypt_string
# End of usual Ansible commands.
