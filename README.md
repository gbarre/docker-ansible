# Docker-ansible

This project aim to build a docker image that will run ansible.

First, the container provide the command line to install the script.
Then, you can use the script to launch the docker and use ansible inside.

```sh
docker run -it gbarre2/docker-ansible:latest
```

Thanks to [François Lafont](https://gitlab.com/flaf) for his very precious help.