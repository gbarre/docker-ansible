FROM ubuntu:24.04

ENV IMAGE_VERSION="3.1"
ENV ANSIBLE_VERSION="10.0.1"
ENV ANSIBLE_LINT_VERSION="24.6.1"

RUN userdel ubuntu; \
    rm -rf /home/ubuntu

# Update ubuntu
RUN set -eux; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -y; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    sudo \
    vim \
    python3-venv \
    openssh-client \
    sshpass \
    nano \
    jq \
    curl \
    iputils-ping \
    dnsutils \
    iproute2 \
    less \
    tree \
    uuid-runtime \
    git \
    telnet; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

# Create ansible home and python environment.
RUN mkdir -p /home/ansible/src; \
    mkdir /home/ansible/.ssh

COPY bash_history /home/ansible/.bash_history
COPY vimrc /etc/skel/.vimrc

RUN chmod 0700 "/home/ansible" "/home/ansible/.ssh"; \
    chmod 0600 "/home/ansible/.bash_history"; \
    addgroup pyenv --system; \
    mkdir "/usr/local/share/pyenv-ansible-${ANSIBLE_VERSION}"; \
    chown "root:pyenv" "/usr/local/share/pyenv-ansible-${ANSIBLE_VERSION}"; \
    chmod 2770 "/usr/local/share/pyenv-ansible-${ANSIBLE_VERSION}"; \
    python3 -m venv "/usr/local/share/pyenv-ansible-${ANSIBLE_VERSION}" --upgrade-deps; \
    . "/usr/local/share/pyenv-ansible-${ANSIBLE_VERSION}/bin/activate"; \
    pip install ansible=="${ANSIBLE_VERSION}"; \
    pip install ansible-lint=="${ANSIBLE_LINT_VERSION}"; \
    pip install ipaddr; \
    pip install netaddr; \
    pip install PyMySQL; \
    pip install jmespath; \
    pip install argcomplete; \
    pip install kubernetes

# Hack for ansible vault
RUN mkdir /var/run/ansible; \
    chmod 0700 /var/run/ansible;

RUN mkdir /install
COPY launch /usr/local/bin/launch

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh; \
    sed -i "s/__SHA256SUM__/$(sha256sum /usr/local/bin/launch | cut -d' ' -f1)/g" /usr/local/bin/entrypoint.sh /usr/local/bin/launch

COPY check_version /usr/local/bin/check_version
RUN chmod +x /usr/local/bin/check_version

ENV TZ="Europe/Paris"

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
